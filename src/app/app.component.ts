import { Component, HostListener } from '@angular/core';
import { MobileService } from './mobile.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'supernova';
  constructor(private mobileService:MobileService){
    this.checkWidth(window.innerWidth);
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.checkWidth(event.target.innerWidth);
  }

  checkWidth(innerWidth){
    if(innerWidth > 991){
      this.mobileService.setTrue();
    } else{
      this.mobileService.setFalse();
    }
  }
}
