import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MobileService {
  toggle: BehaviorSubject<boolean>;
  constructor() {
    this.toggle = new BehaviorSubject(false);
  }
  public setFalse(): void {
    this.toggle.next(false);
  }

  public setTrue(): void{
    this.toggle.next(true);
  }

  public setVal(val: boolean): void{
    this.toggle.next(val);
  }
}
