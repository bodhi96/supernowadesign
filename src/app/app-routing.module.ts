import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/authentication/login/login.component';
import { TeenPattiComponent } from './components/games/teen-patti/teen-patti.component';
import { JokerComponent } from './components/games/joker/joker.component';
import { CardRaceComponent } from './components/games/card-race/card-race.component';
import { TeenPattiThreePlayerComponent } from './components/games/teen-patti-three-player/teen-patti-three-player.component';
import { BaccaratComponent } from './components/games/baccarat/baccarat.component';
import { AndharBaharComponent } from './components/games/andhar-bahar/andhar-bahar.component';
import { AkberRomeoWalterComponent } from './components/games/akber-romeo-walter/akber-romeo-walter.component';
import { TeenPattiSpeedyComponent } from './components/games/teen-patti-speedy/teen-patti-speedy.component';
import { ThirtytwoCardsComponent } from './components/games/thirtytwo-cards/thirtytwo-cards.component';
import { RockPaperScissorsComponent } from './components/games/rock-paper-scissors/rock-paper-scissors.component';
import { WorlyMatkaComponent } from './components/games/worly-matka/worly-matka.component';
import { LuckySevenComponent } from './components/games/lucky-seven/lucky-seven.component';
import { DragonTigerComponent } from './components/games/dragon-tiger/dragon-tiger.component';
import { BlackJackComponent } from './components/powergames/black-jack/black-jack.component';

export const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'teen-patti',
    component: TeenPattiComponent,
  },
  {
    path: "teen-patti-3",
    component: TeenPattiThreePlayerComponent
  },
  {
    path: "teen-patti-speedy",
    component: TeenPattiSpeedyComponent
  },
  {
    path: 'joker',
    component: JokerComponent,
  },
  {
    path: 'card-race',
    component: CardRaceComponent,
  },
  {
    path: 'baccarat',
    component: BaccaratComponent,
  },
  {
    path: 'teen-patti-speedy',
    component: TeenPattiSpeedyComponent,
  },
  {
    path: 'andhar-bahar',
    component: AndharBaharComponent,
  },
  {
    path: 'akber-romeo-walter',
    component: AkberRomeoWalterComponent,
  },
  {
    path: '32-cards',
    component: ThirtytwoCardsComponent,
  },
  {
    path: 'rock-paper-scissors',
    component: RockPaperScissorsComponent,
  },
  {
    path: 'lucky-seven',
    component: LuckySevenComponent,
  },
  {
    path: 'worly-matka',
    component: WorlyMatkaComponent,
  },
  {
    path: 'dragon-tiger',
    component: DragonTigerComponent,
  },
  {
    path: 'powergames/black-jack',
    component: BlackJackComponent,
  },
  
  {
    path: '',
    redirectTo: 'worly-matka',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
