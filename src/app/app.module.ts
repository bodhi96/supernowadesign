import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from './components/authentication/login/login.component';
import { TeenPattiComponent } from './components/games/teen-patti/teen-patti.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { JokerComponent } from './components/games/joker/joker.component';
import { DealerSliderComponent } from './components/reusable/dealer-slider/dealer-slider.component';
import { BetSliderComponent } from './components/reusable/bet-slider/bet-slider.component';

import { SwiperModule } from 'swiper/angular';
import { CardRaceComponent } from './components/games/card-race/card-race.component';
import { HeaderComponent } from './components/layout/header/header.component';
import { LiveGamesBarComponent } from './components/reusable/live-games-bar/live-games-bar.component';
import { TeenPattiThreePlayerComponent } from './components/games/teen-patti-three-player/teen-patti-three-player.component';
import { BaccaratComponent } from './components/games/baccarat/baccarat.component';
import { AndharBaharComponent } from './components/games/andhar-bahar/andhar-bahar.component';
import { AkberRomeoWalterComponent } from './components/games/akber-romeo-walter/akber-romeo-walter.component';
import { TeenPattiSpeedyComponent } from './components/games/teen-patti-speedy/teen-patti-speedy.component';
import { VideoComponent } from './components/reusable/video/video.component';
import { ThirtytwoCardsComponent } from './components/games/thirtytwo-cards/thirtytwo-cards.component';
import { RockPaperScissorsComponent } from './components/games/rock-paper-scissors/rock-paper-scissors.component';
import { LuckySevenComponent } from './components/games/lucky-seven/lucky-seven.component';
import { WorlyMatkaComponent } from './components/games/worly-matka/worly-matka.component';
import { DragonTigerComponent } from './components/games/dragon-tiger/dragon-tiger.component';
import { CirclesliderComponent } from './components/reusable/circleslider/circleslider.component';
import { BlackJackComponent } from './components/powergames/black-jack/black-jack.component';
import { NgxWheelModule } from 'ngx-wheel';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TeenPattiComponent,
    TeenPattiThreePlayerComponent,
    JokerComponent,
    DealerSliderComponent,
    BetSliderComponent,
    CardRaceComponent,
    HeaderComponent,
    LiveGamesBarComponent,
    BaccaratComponent,
    AndharBaharComponent,
    AkberRomeoWalterComponent,
    TeenPattiSpeedyComponent,
    VideoComponent,
    ThirtytwoCardsComponent,
    RockPaperScissorsComponent,
    LuckySevenComponent,
    WorlyMatkaComponent,
    DragonTigerComponent,
    CirclesliderComponent,
    BlackJackComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    SlickCarouselModule,
    SwiperModule,
    NgxWheelModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
