import { AfterViewInit, Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-joker',
  templateUrl: './joker.component.html',
  styleUrls: ['./joker.component.scss']
})
export class JokerComponent implements OnInit, AfterViewInit {

  constructor() { }

  ngOnInit(): void {
    window.addEventListener('resize', (event) => {
      this.updateViewAccordingToScreenSize("resize")
    });
    window.addEventListener('load', (event) => {
      this.updateViewAccordingToScreenSize("load")
    });
  }

  ngAfterViewInit() {
  }

  displayStatsCard = false;
  bookShow = false;
  placeBetCard = false;
  placeBetCardClone = false;
  placeBetCardState = false;
  isMediumScreen = false;
  isScreenLessThanLarge = false;
  isAccordionOpen = false;
  confattishow = false;

  toggleStatsCard() {
    this.displayStatsCard = !this.displayStatsCard
    this.isAccordionOpen = this.displayStatsCard
  }

  displayPlaceBetCard() {
    if (!this.isMediumScreen){
      this.placeBetCard = true
      this.placeBetCardClone = false
      this.placeBetCardState = true;
    }
  }

  onBackFromPlaceBetCard(){
    this.placeBetCardState = false;
  }

  updateViewAccordingToScreenSize(event) {
    if (window.innerWidth > 767 && window.innerWidth < 992) {
      this.placeBetCard = false;
      this.placeBetCardClone = true;
      this.isMediumScreen = true;
    } else {
      this.isMediumScreen = false;
      this.placeBetCard = true;
      this.placeBetCardClone = false;
    }

    if (window.innerWidth < 992) {
      this.isScreenLessThanLarge = true
    } else {
      this.isScreenLessThanLarge = false
    }
  }

  bettingHistoryTable = [
    { id: 1 },
    { id: 2 },
    { id: 3 },
    { id: 4 },
    { id: 5 },
    { id: 6 },
    { id: 7 },
  ]
  gameResultCircles = [
    { id: 1, name: 'A', active: false },
    { id: 2, name: 'B', active: false },
    { id: 3, name: 'A', active: false },
    { id: 4, name: 'C', active: false },
    { id: 5, name: 'E', active: false },
    { id: 6, name: 'D', active: true },
    { id: 7, name: 'F', active: false },
  ]

}
