import { AfterViewInit, Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
declare var NanoPlayer;

@Component({
  selector: 'app-akber-romeo-walter',
  templateUrl: './akber-romeo-walter.component.html',
  styleUrls: ['./akber-romeo-walter.component.scss']
})
export class AkberRomeoWalterComponent implements OnInit,AfterViewInit {

  constructor() { }

  ngOnInit(): void {
    window.addEventListener('resize', (event) => {
      this.updateViewAccordingToScreenSize('resize');
    });
    window.addEventListener('load', (event) => {
      this.updateViewAccordingToScreenSize('load');
    });
  }
  ngAfterViewInit() {
    this.player = new NanoPlayer("playerDiv");
    this.player.setup(this.config).then(function (config) {
      console.log("setup success");
      console.log($("#playerDiv video").height());
    }, function (error) {
      alert(error.message);
    });
  }
  displayStatsCard = false;
  bookShow = false;
  placeBetCard = false;
  isMediumScreen = false;
  isScreenLessThanLarge = false;
  isAccordionOpen = false;

  player;
  defaultUrl = "rtmp://bintu-play.nanocosmos.de/play";
  defaultServer = {
    "websocket": "wss://bintu-play.nanocosmos.de:443/h5live/stream.mp4",
    "hls": "https://bintu-play.nanocosmos.de:443/h5live/http/playlist.m3u8",
    "progressive": "https://bintu-play.nanocosmos.de:443/h5live/http/stream.mp4"
  };
  streamNames = [
    "AZyne-3AjTw",
    "AZyne-9P1MO",
    "AZyne-4lJhK",
    "AZyne-IdgwS"
  ];
  toggleStatsCard() {
    this.displayStatsCard = !this.displayStatsCard
    this.isAccordionOpen = this.displayStatsCard
  }
  displayPlaceBetCard(val) {
    if (!this.isMediumScreen)
      this.placeBetCard = val
  }
  updateViewAccordingToScreenSize(event) {
    if (window.innerWidth > 767 && window.innerWidth < 992) {
      this.displayPlaceBetCard(true)
      this.isMediumScreen = true;
    } else {
      if (event != "resize")
        this.displayPlaceBetCard(false)
      this.isMediumScreen = false;
    }
    if (window.innerWidth < 992) {
      this.isScreenLessThanLarge = true
    } else {
      this.isScreenLessThanLarge = false
    }
  }

  gameResultCircles = [
    { id: 1, name: 'A', active: false },
    { id: 2, name: 'R', active: false },
    { id: 3, name: 'R', active: false },
    { id: 4, name: 'A', active: false },
    { id: 5, name: 'W', active: false },
    { id: 6, name: 'W', active: true },
    { id: 7, name: 'W', active: false },
  ];

  config = {
    "source": {
      "entries": [
        {
          "index": 0,
          "label": "stream 1",
          "info": {
            "bitrate": 0,
            "width": 0,
            "height": 0,
            "framerate": 0
          },
          "h5live": {
            "rtmp": {
              "url": this.defaultUrl,
              "streamname": this.streamNames[0]
            },
            "server": this.defaultServer
          }
        },
        {
          "index": 1,
          "label": "stream 2",
          "info": {
            "bitrate": 1200,
            "width": 1280,
            "height": 720,
            "framerate": 30
          },
          "h5live": {
            "rtmp": {
              "url": this.defaultUrl,
              "streamname": this.streamNames[1]
            },
            "server": this.defaultServer
          }
        },
        {
          "index": 2,
          "label": "stream 3",
          "info": {
            "bitrate": 800,
            "width": 852,
            "height": 480,
            "framerate": 30
          },
          "h5live": {
            "rtmp": {
              "url": this.defaultUrl,
              "streamname": this.streamNames[2]
            },
            "server": this.defaultServer
          }
        },
        {
          "index": 3,
          "label": "stream 4",
          "info": {
            "bitrate": 400,
            "width": 640,
            "height": 360,
            "framerate": 25
          },
          "h5live": {
            "rtmp": {
              "url": this.defaultUrl,
              "streamname": this.streamNames[3]
            },
            "server": this.defaultServer
          }
        }
      ],
      "options": {
        "adaption": {
          "rule": "deviationOfMean2"
        }
      },
      "startIndex": 1
    },
    "playback": {
      "autoplay": true,
      "automute": true,
      "muted": true
    },
    "style": {
      "displayMutedAutoplay": true
    },
    "metrics": {
      "accountId": "nanocosmos1",
      "accountKey": "nc1wj472649fkjah",
      "userId": "nanoplayer-demo",
      "eventId": "nanocosmos-demo",
      "statsInterval": 10,
      "customField1": "demo",
      "customField2": "public",
      "customField3": "online resource"
    }
  };

}
