import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  isDayMode = false

  constructor(@Inject(DOCUMENT) private document: Document) {}

  ngOnInit(): void {
    this.isDayMode = JSON.parse(localStorage.getItem("DisplayMode"))
    this.changeDisplayMode(this.isDayMode)
  }

  ngOnDestroy(){
    localStorage.removeItem("DisplayMode")
  }

  handleDisplayMode(e){
    console.log(e.target.checked);
    this.changeDisplayMode(e.target.checked)
  }

  changeDisplayMode(value){
    localStorage.setItem("DisplayMode", JSON.stringify(value))
    if (value) {
      this.document.body.classList.replace("night-mode", "day-mode");
    }else{
      this.document.body.classList.replace("day-mode", "night-mode");
    }
  }
}
