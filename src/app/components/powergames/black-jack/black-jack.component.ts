import { Component, OnInit } from '@angular/core';
import { MobileService } from 'src/app/mobile.service';

@Component({
  selector: 'app-black-jack',
  templateUrl: './black-jack.component.html',
  styleUrls: ['./black-jack.component.scss']
})
export class BlackJackComponent implements OnInit {
  public showButton = false;
  constructor(public mobileService: MobileService) { }

  ngOnInit(): void {
    this.mobileService.toggle.subscribe(res=>{
      this.showButton= res;
      this.updateViewAccordingToScreenSize()
    })
  }
  
  infobox = false;
  fairbox = false;
  winnerbox = false;
  blackjackButtons = true;

  buttonshowonmobile = false;
  slidershowonmobile = false;
  isMediumScreen = false;
  isMobile = false;
  infoActive : boolean = false;
  fairActive : boolean = false;

  toggleinfo() {
    this.infobox = !this.infobox
    this.fairbox = false;
    this.infoActive = !this.infoActive;
  }
  togglefair() {
    this.fairbox = !this.fairbox
    this.infobox = false;
    this.fairActive = !this.fairActive;  
  }
  toggleWinner() {
    this.winnerbox = !this.winnerbox
    this.blackjackButtons = false
    
  }
  playagain() {
    this.winnerbox = false
    this.blackjackButtons = true
    
  }
  
  updateViewAccordingToScreenSize() {
    if (window.innerWidth > 320 && window.innerWidth < 992) {
      this.isMobile = true;
      
    } else {
      this.isMobile = false;
    }
  }
}
