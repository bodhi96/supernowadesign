import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BetSliderComponent } from './bet-slider.component';

describe('BetSliderComponent', () => {
  let component: BetSliderComponent;
  let fixture: ComponentFixture<BetSliderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BetSliderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BetSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
