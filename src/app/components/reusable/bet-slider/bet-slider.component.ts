import { Component, OnInit } from '@angular/core';
import SwiperCore, {
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  SwiperOptions,
} from 'swiper/core';
SwiperCore.use([Navigation, Pagination, Scrollbar, A11y]);

@Component({
  selector: 'app-bet-slider',
  templateUrl: './bet-slider.component.html',
  styleUrls: ['./bet-slider.component.scss'],
})
export class BetSliderComponent implements OnInit {
  config: SwiperOptions = {
    spaceBetween: 15,
    slidesPerView: 4,
    navigation: true,
    loop: true,
    pagination:true
    
  };  
  breakpoints = {
    320: { slidesPerView: 3, spaceBetween: 20 },
    576: { slidesPerView: 4, spaceBetween: 40 },
    768: { slidesPerView: 3, spaceBetween: 40 },
    992: { slidesPerView: 2, spaceBetween: 50 },
    1200: { slidesPerView: 2, spaceBetween: 15 },
    1400: { slidesPerView: 3, spaceBetween: 15 },
  };
  slides = [
    {name: '500'},
    {name: '1000'},
    {name: '5000'},
    {name: '10K'},
    {name: '50K'},
    {name: '100K'},
    {name: '500K'},
    {name: '5M'},
  ]
  constructor() {}

  ngOnInit(): void {}
}
