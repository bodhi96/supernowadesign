import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DealerSliderComponent } from './dealer-slider.component';

describe('DealerSliderComponent', () => {
  let component: DealerSliderComponent;
  let fixture: ComponentFixture<DealerSliderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DealerSliderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DealerSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
