import { Component, OnInit } from '@angular/core';
import SwiperCore, {
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  SwiperOptions,
} from 'swiper/core';
SwiperCore.use([Navigation, Pagination, Scrollbar, A11y]);
@Component({
  selector: 'app-dealer-slider',
  templateUrl: './dealer-slider.component.html',
  styleUrls: ['./dealer-slider.component.scss'],
})
export class DealerSliderComponent implements OnInit {
  sliderMove(swiper) {
    console.log(swiper);
  }
  config: SwiperOptions = {
    spaceBetween: 10,
    slidesPerView: 8,
    
    loop: true,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    allowTouchMove:true,
  };
  
  breakpoints = {
    320: { slidesPerView: 4, spaceBetween: 6, },
    576: { slidesPerView: 5 , spaceBetween: 10,},
    768: { slidesPerView: 5 },
    992: { slidesPerView: 5 },
    1200: { slidesPerView: 6 },
    1350: { slidesPerView: 7 },
    1500: { slidesPerView: 8 },
  };
  
  slides = [
    {name: '32 Cards'},
    {name: 'Akbar Romeo Walter'},
    {name: 'Andar Bahar'},
    {name: 'Teen Patti'},
    {name: 'Teen Patti 3P'},
    {name: 'Teen Patti Speedy'},
    {name: 'Lucky 7'},
    {name: 'Rock Paper Scissors'},
    {name: 'Dragon Tiger'},
    {name: 'Worli Matka'},
    {name: 'Joker'},
    {name: 'Baccarat'},
    {name: 'Card Race'},
  ]
  constructor() {}

  ngOnInit(): void {}

}
