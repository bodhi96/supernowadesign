import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-live-games-bar',
  templateUrl: './live-games-bar.component.html',
  styleUrls: ['./live-games-bar.component.scss']
})
export class LiveGamesBarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  liveGames = [
    {name: '32 Cards'},
    {name: 'Akbar Romeo Walter'},
    {name: 'Andar Bahar'},
    {name: 'Teen Patti'},
    {name: 'Teen Patti 3P'},
    {name: 'Teen Patti Speedy'},
    {name: 'Lucky 7'},
    {name: 'Rock Paper Scissors'},
    {name: 'Dragon Tiger'},
    {name: 'Worli Matka'},
    {name: 'Joker'},
    {name: 'Baccarat'},
    {name: 'Card Race'},
  ]

}
