import { Component, OnInit } from '@angular/core';
import { MobileService } from 'src/app/mobile.service';
​
declare var $: any;
​
@Component({
  selector: 'app-circleslider',
  templateUrl: './circleslider.component.html',
  styleUrls: ['./circleslider.component.scss']
})
export class CirclesliderComponent implements OnInit {
  slideData = [100, 200,300,400,100,500,200,100];
  constructor(private mobileService :MobileService) { }
​
  ngOnInit(): void {
​
  }
  goLeft(){
    var element = this.slideData.pop();
    this.slideData.unshift(element);
  }
  goRight(){
    var element = this.slideData.shift();
    this.slideData.push(element);
  }

  public showSideButtonsOnMobile() {
      this.mobileService.setVal(true);
  }
​
}
