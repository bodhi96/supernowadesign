import { AfterViewInit, Component, OnInit } from '@angular/core';

declare var NanoPlayer;

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss']
})
export class VideoComponent implements OnInit, AfterViewInit {

  player;
  defaultUrl = "rtmp://bintu-play.nanocosmos.de/play";
  defaultServer = {
    "websocket": "wss://bintu-play.nanocosmos.de:443/h5live/stream.mp4",
    "hls": "https://bintu-play.nanocosmos.de:443/h5live/http/playlist.m3u8",
    "progressive": "https://bintu-play.nanocosmos.de:443/h5live/http/stream.mp4"
  };
  streamNames = [
    "AZyne-3AjTw",
    "AZyne-9P1MO",
    "AZyne-4lJhK",
    "AZyne-IdgwS"
  ];

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.player = new NanoPlayer("playerDiv");
    this.player.setup(this.config).then(function (config) {
      console.log("setup success");
    }, function (error) {
      alert(error.message);
    });
  }

  config = {
    "source": {
      "entries": [
        {
          "index": 0,
          "label": "stream 1",
          "info": {
            "bitrate": 0,
            "width": 0,
            "height": 0,
            "framerate": 0
          },
          "h5live": {
            "rtmp": {
              "url": this.defaultUrl,
              "streamname": this.streamNames[0]
            },
            "server": this.defaultServer
          }
        },
        {
          "index": 1,
          "label": "stream 2",
          "info": {
            "bitrate": 1200,
            "width": 1280,
            "height": 720,
            "framerate": 30
          },
          "h5live": {
            "rtmp": {
              "url": this.defaultUrl,
              "streamname": this.streamNames[1]
            },
            "server": this.defaultServer
          }
        },
        {
          "index": 2,
          "label": "stream 3",
          "info": {
            "bitrate": 800,
            "width": 852,
            "height": 480,
            "framerate": 30
          },
          "h5live": {
            "rtmp": {
              "url": this.defaultUrl,
              "streamname": this.streamNames[2]
            },
            "server": this.defaultServer
          }
        },
        {
          "index": 3,
          "label": "stream 4",
          "info": {
            "bitrate": 400,
            "width": 640,
            "height": 360,
            "framerate": 25
          },
          "h5live": {
            "rtmp": {
              "url": this.defaultUrl,
              "streamname": this.streamNames[3]
            },
            "server": this.defaultServer
          }
        }
      ],
      "options": {
        "adaption": {
          "rule": "deviationOfMean2"
        }
      },
      "startIndex": 1
    },
    "playback": {
      "autoplay": true,
      "automute": true,
      "muted": true
    },
    "style": {
      "displayMutedAutoplay": true
    },
    "metrics": {
      "accountId": "nanocosmos1",
      "accountKey": "nc1wj472649fkjah",
      "userId": "nanoplayer-demo",
      "eventId": "nanocosmos-demo",
      "statsInterval": 10,
      "customField1": "demo",
      "customField2": "public",
      "customField3": "online resource"
    }
  };

}
